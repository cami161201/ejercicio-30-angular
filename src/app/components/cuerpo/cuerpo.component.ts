import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cuerpo',
  templateUrl: './cuerpo.component.html',
  styleUrls: ['./cuerpo.component.css']
})
export class CuerpoComponent implements OnInit {
  tamano: number = 30
  text: string = '';
  textErr: string = 'A llegado al limite'
  constructor() { }

  ngOnInit(): void {

  }

  aumentar(){
    this.tamano = this.tamano + 5
    if (this.tamano === 500){
      this.text = this.textErr
    }
  }

  disminuir(){
    this.tamano = this.tamano - 5;
    if(this.tamano === 5){
      this.text = this.textErr
    }
  }

}
